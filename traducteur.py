#!/usr/bin/python3
# coding=utf-8

from netfilterqueue import NetfilterQueue
import socket
from scapy.all import *

# À modifier selon usage
traducteur_IPv4 =  "10.0.0.3"
IF_internet = 'eno1'


traducteur_IPv6 = "2001:2:3:4501:b8be:deff:fead:beef"
serveur_IPv4 = "10.0.1.254"
serveur_IPv6 = "2001:5:3:4501:dcad:beff:feef:beba"
client_IPv6 = "2001:2:3:4501:dcad:beff:feef:babe"
adresse_traducteur_cote_serveur = '10.0.1.1'

IF_client = 'bridge_ipv6'
IF_serveur = 'swserv'

historique_fin= {}
historique = {}
dns = {}
dns["domaine"] = (serveur_IPv4,serveur_IPv6)
subprocess.run("ip6tables -t mangle -A PREROUTING -i bridge_ipv6 -j NFQUEUE --queue-num 0",shell=True)
subprocess.run("iptables -t mangle -A PREROUTING -i swserv -j NFQUEUE --queue-num 0",shell=True)
subprocess.run("iptables -t mangle -A PREROUTING -d "+str(traducteur_IPv4)+" -j NFQUEUE --queue-num 0",shell=True)
def traite_paquet(payload):
    verdict = False
    # le paquet est fourni sous forme d'une séquence d'octet, il faut l'importer
    data = payload.get_payload()

    # il faut identifier sa nature IPv6 ou IPv4
    premier_quartet = hex(data[0])[2] # 2 car on ignore le 0x devant
    pkt = None
    if (premier_quartet == '4'):
        # paquet IPv4
        pkt = IP(data)

        if TCP in pkt:
            # On ne s'en occupe que si c'est dans notre DNS
            source = None
            for domain in dns:
                if pkt[IP].src == dns[domain][0]:
                    source = dns[domain][1] # On fait correspondre avec le DNS AAAA
            if source:
                # On ne s'en occupe que si on est concernés
                if pkt[IP].src in historique and pkt[TCP].dport in historique[pkt[IP].src]:
                    #Permet de marqué le paquet en cas de fin
                    if "F" in pkt[TCP].flags:
                        historique_fin[pkt[IP].src] = [pkt[TCP].dport]
                    nouveau_paquet = Ether()/IPv6(src = source, dst = client_IPv6)/pkt[TCP]

                    # On recalcule le checksum TCP
                    del(nouveau_paquet[TCP].chksum)
                    
                    # On envoie l'équivalent IPv6
                    sendp(nouveau_paquet, iface = IF_client, verbose = False)
                    # On supprime le paquet original
                    payload.drop()
                    verdict = True
    else:
        # paquet IPv6
        pkt = IPv6(data)

        if UDP in pkt and pkt[UDP].dport == 53:
            # On veut se rappeler des équivalents DNS
            domain_name = pkt[DNS].qd.qname.decode('utf-8')

            # On écupère son IPv4 et son IPv6
            reponse_dns6 = sr1(IP(dst="1.1.1.1")/UDP()/DNS(qd=DNSQR(qname = domain_name, qtype="AAAA")), iface = IF_internet, verbose = False).an
            reponse_dns4 = sr1(IP(dst="1.1.1.1")/UDP()/DNS(qd=DNSQR(qname = domain_name, qtype="A")), iface = IF_internet, verbose = False).an
            adresse_ipv4 = None
            adresse_ipv6 = None

            # On ne récupère que les adresses, pas les CNAME
            i = 0
            while True:
                if reponse_dns6[i].type == 28: # AAAA
                    adresse_ipv6 = reponse_dns6[i].rdata
                    break
                i += 1
            i = 0
            while True:
                if reponse_dns4[i].type == 1: # A
                    adresse_ipv4 = reponse_dns4[i].rdata
                    break
                i += 1

            # On sauvegarde ces adresses
            dns[domain_name] = (adresse_ipv4, adresse_ipv6)

            # On répond au client (avec l'adresse IPv6
            # Source: https://jasonmurray.org/posts/2020/scapydns/
            layer_dns = DNS(
                id = pkt[DNS].id,
                qd = pkt[DNS].qd,
                aa = 1,
                rd = 0,
                qr = 1,
                qdcount = 1,
                ancount = 1,
                nscount = 0,
                arcount = 0,
                ar = DNSRR (
                    rrname = domain_name,
                    type = 'AAAA',
                    ttl = 600,
                    rdata = adresse_ipv6
                )
            )

            # On envoie au client l'IPv6 du serveur
            reponse = Ether()/IPv6(src = pkt[IPv6].dst, dst = client_IPv6)/UDP(dport = pkt[UDP].sport, sport = pkt[UDP].dport)/layer_dns

            sendp(reponse, iface = IF_client, verbose = False)


            verdict = True
            payload.drop()

        if TCP in pkt:

            # On ne s'en occupe que si c'est dans notre DNS
            dest = None
            for domain in dns:
                if pkt[IPv6].dst == dns[domain][1]:
                    dest = dns[domain][0] # On fait correspondre avec le DNS A
            if dest:
                nouveau_paquet = Ether()/IP(src = traducteur_IPv4, dst = dest)/pkt[TCP]
                # On recalcule le checksum TCP
                del(nouveau_paquet[TCP].chksum)

                # On veut se souvenir des paquets concernés par le traducteur
                if nouveau_paquet[IP].dst not in historique:
                    historique[nouveau_paquet[IP].dst] = []
                if nouveau_paquet[TCP].sport not in historique[nouveau_paquet[IP].dst]:
                    historique[nouveau_paquet[IP].dst].append(nouveau_paquet[TCP].sport)
                #Permet de supprimer la cible du traducteur si il a été marqué plus tot
                if nouveau_paquet[IP].dst in historique_fin and nouveau_paquet[TCP].dport in historique_fin[nouveau_paquet[IP].dst]:
                    historique[nouveau_paquet[IP].dst].remove(nouveau_paquet[TCP].dport)
                    historique_fin[nouveau_paquet[IP].dst].remove(nouveau_paquet[TCP].dport)

                # On envoie soit à notre serveur, soit sur internet
                if nouveau_paquet[IP].dst == serveur_IPv4:
                    nouveau_paquet[IP].src = adresse_traducteur_cote_serveur
                    sendp(nouveau_paquet, iface = IF_serveur, verbose = False)
                else:
                    sendp(nouveau_paquet, iface = IF_internet, verbose = False)
                # On supprime le paquet original
                payload.drop()
                verdict = True

    if not verdict:
        # Sinon, on accepte le paquet
        payload.accept()


 

nfqueue = NetfilterQueue()
nfqueue.bind(0, traite_paquet)

try:
    nfqueue.run()
except KeyboardInterrupt as e:

    subprocess.run("ip6tables -t mangle -D PREROUTING -i bridge_ipv6 -j NFQUEUE --queue-num 0", shell=True)
    subprocess.run("iptables -t mangle -D PREROUTING -d " + str(traducteur_IPv4) + " -j NFQUEUE --queue-num 0",shell=True)
    pass
    subprocess.run("iptables -t mangle -D PREROUTING -i swserv -j NFQUEUE --queue-num 0",shell=True)

nfqueue.unbind()

