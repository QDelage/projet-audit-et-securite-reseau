# Project certificate authority - public

Public repository of one of my first-year of Master degree projects about a translator IPv6 - IPv4 for the TCP protocol, realised in pair (anonymised).

<img src="screenshots/google.png">

## Getting Started

### Prerequisites

What things you need to install the software and how to install them

You will need these Python3 libraries:

* netfilterqueue
* scapy

You will also need radvd for use with the ip netns lab, that relies on openvswitch.

### Usage

First, build the lab, then launch radvd. Finally run the translator, and you can use it.

Steps:

```
./build_architecture
./radvd.sh
```

Finally, run the translator

```
python traducteur.py
```

Finally, you can send tcp requests from the netns h1 (to a real server that has dns entries, or to the netns server - see the screenshots.

![Usage screenshot](screenshots/socat.png "Usage screenshot")

## Built With

* [NetFilterQueue](https://github.com/oremanj/python-netfilterqueue) - Python library to use the netfilter queue
* [Scapy](https://scapy.net/) - Packet manipulation program, used as a library

## Authors

* **Quentin DELAGE** - *Initial work* - [QDelage](https://gitlab.com/QDelage)
* **Pair** - *Initial work* - anonymised

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

